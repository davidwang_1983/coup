# Coup
1 Unity Version 2021.2.18f1, please use 1920*1080 fix resolution to play
2 Run SampleScene to start the game, Use the 'p' key to move to the next round.
3 Only show 'logError' in the console; it will display all notifications.
4 The entire game could be validated with 100% logic if the 'time.time' tick is replaced with a 100ms tick. This would make it easier for future anti-cheating systems
5 I used a lot of singletons to save time. In the production process, I would prefer to use a subscribe/publisher model.
6 I should have implemented 'dispose' to clean up, but for the prototype, I have only left an empty function for now.
7 For exchange action, pls check the console output to see the new two cards options.
8 I included some basic weight calculation and speculation in the AI logic, which can be extended in the future.


