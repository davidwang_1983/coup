using System.Collections;
using System.Collections.Generic;
using UnityEditor.Search;
using UnityEngine;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    Game game;
    Canvas canvas;
    private void Awake()
    {
        game = GameObject.FindObjectOfType<Game>();
        canvas = GetComponent<Canvas>();
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show()
    {
        canvas.enabled = true;
    }

    public void Hide()
    {
        canvas.enabled = false;
    }

    public void OnButton2()
    {
        game.StartGame(2);
    }
    public void OnButton3()
    {
        game.StartGame(3);
    }
    public void OnButton4()
    {
        game.StartGame(4);
    }
    public void OnButton5()
    {
        game.StartGame(5);
    }
    public void OnButton6()
    {
        game.StartGame(6);
    }
}
