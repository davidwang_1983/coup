using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    // Start is called before the first frame update
    public Image card1;
    public Image card2;
    public Image open1;
    public Image open2;
    public TextMeshProUGUI coin;
    public TextMeshProUGUI Name;
    void Start()
    {
    }

    public void RefreshUI(Player p)
    {
        coin.text = p.Coin.ToString();
        Name.text = p.Name;
        var cards = p.GetCards();
        for(int i = 0; i < cards.Length; ++i)
        {
            var uiCard = i == 0 ? card1 : card2;
            var openMark = i == 0 ? open1 : open2;
            if (p.IsAI())
            {
                if (cards[i].Open)
                {
                    openMark.gameObject.SetActive(true);
                    SetCardColor(uiCard, cards[i].Card);
                }
                else
                {
                    openMark.gameObject.SetActive(false);
                    SetCardColor(uiCard, CardType.None);
                }
            }
            else
            {
                openMark.gameObject.SetActive(cards[i].Open);
                SetCardColor(uiCard, cards[i].Card);
            }
        }

    }

    void SetCardColor(Image card, CardType type)
    {
        if(type == CardType.None)
        {
            card.color = new Color(0, 0, 0, 0);
        }
        else if(type == CardType.Contessa)
        {
            card.color = Color.red;
        }
        else if (type == CardType.Ambassador)
        {
            card.color = Color.green;
        }
        else if (type == CardType.Duke)
        {
            card.color = new Color(0.5f, 0, 0.5f, 1);
        }
        else if (type == CardType.Captain)
        {
            card.color = Color.blue;
        }
        else if (type == CardType.Assassin)
        {
            card.color = Color.black;
        }
    }

    public void ShowCanvas(bool bShow)
    {
        gameObject.SetActive(bShow);
    }

    public void SetCurrent(bool bShow)
    {
        Name.color = bShow ? Color.green : Color.white;
    }
}
