using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    Menu menu;
    UIBoard board;

    private void Awake()
    {
        menu = GetComponentInChildren<Menu>();
        board = GetComponentInChildren<UIBoard>();
    }
    void Start()
    {

        menu.Show();
        board.Hide();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame(int maxPlayer)
    {
        menu.Hide();
        board.Show(maxPlayer);
    }

    public void EndGame()
    {
        menu.Show();
        board.Hide();
    }
}
