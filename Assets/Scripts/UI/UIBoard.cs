using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UIBoard : MonoBehaviour
{
    Canvas canvas;
    int _maxPlayer;
    Player _humanPlayer;
    int _choiceStep;
    public static UIBoard instance;
    public List<PlayerUI> players;

    public List<Button> choices;
    public List<TextMeshProUGUI> choiceTexts;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        instance = this;
        for (int i = 0; i < choices.Count; ++i)
        {
            Button button = choices[i];
            int idx = i;
            button.onClick.AddListener(()=>OnChoiceClick(idx));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show(int maxPlayer)
    {
        _maxPlayer = maxPlayer;
        canvas.enabled = true;
        for (int i = 0; i < choices.Count; ++i)
        {
            Button button = choices[i];
            button.gameObject.SetActive(false);
        }
        for (int i = 0; i < players.Count; ++i)
        {
            players[i].ShowCanvas(i < maxPlayer);
        }
    }

    public void Hide()
    {
        canvas.enabled = false;
    }

    public void UpdatePlayer(Player player)
    {
        players[player.Index].RefreshUI(player);
    }

    public void SetCurrentPlayer(Player player)
    {
        for(int i = 0; i < _maxPlayer; ++i)
        {
            players[i].SetCurrent(player.Index == i);
        }
    }

    void ClearChoiceUI()
    {
        for (int i = 0; i < choices.Count; ++i)
        {
            Button button = choices[i];
            button.gameObject.SetActive(false);
        }
    }
    public void ShowChoice(Player p, List<ActionType> actionList)
    {
        _humanPlayer = p;
        _choiceStep = 1;
        for (int i = 0; i < choices.Count; ++i)
        {
            Button button = choices[i];
            if(i < actionList.Count)
            {
                button.gameObject.SetActive(true);
                choiceTexts[i].text = actionList[i].ToString();
                
            }
            else
            {
                button.gameObject.SetActive(false);
            }
        }
    }

    public void ShowSecondChoice(Player p, List<Player> playerList)
    {
        _humanPlayer = p;
        _choiceStep = 2;
        for (int i = 0; i < choices.Count; ++i)
        {
            Button button = choices[i];
            if (i < playerList.Count)
            {
                button.gameObject.SetActive(true);
                choiceTexts[i].text = playerList[i].Name;

            }
            else
            {
                button.gameObject.SetActive(false);
            }
        }
    }

    void OnChoiceClick(int idx)
    {
        ClearChoiceUI();
        if (_choiceStep == 1)
        {
            var secondList = _humanPlayer.MakeFirstChoice(idx);
            if (secondList.Count > 0)
            {
                ShowSecondChoice(_humanPlayer, secondList);
            }
            else
            {
                _humanPlayer.SetFinishInput();
            }
        }
        else if(_choiceStep == 2)
        {
            _humanPlayer.MakeSecondChoice(idx);
            _humanPlayer.SetFinishInput();
        }

    }
}
