using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    Board _board;
    UIController _uiController;

    public static Game instance;
    void Start()
    {
        _uiController = GameObject.FindObjectOfType<UIController>();
        instance = this;
    }

    public void StartGame(int number)
    {
        if(_board != null)
        {
            Debug.LogError("last game not end");
            return;
        }
        _board = new Board(number);
        _uiController.StartGame(number);
    }

    public void EndGame()
    {
        _uiController.EndGame();
        _board.Dispose();
        _board = null;
    }

    // Update is called once per frame
    void Update()
    {
        if(_board != null)
        {
            _board.Tick();
            if (Input.GetKeyDown("p"))
            {
                StateTurnStart start = _board.GetStateMgr().GetState() as StateTurnStart;
                if(start != null)
                {
                    start.TestContinue();
                }
            }
        }
    }


}
