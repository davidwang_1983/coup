using System;
using System.Collections;
using System.Collections.Generic;
using TMPro.EditorUtilities;
using Unity.VisualScripting;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public enum ActionType
{
    ChooseStart,
    Income,
    ForeignAid,
    Coup,
    Tax,
    Assassinate,
    Steal,
    Exchange,
    ChooseMax,
    CounterStart,
    NoCounter,
    BlockForeignAid,
    BlockAssassinate,
    BlockStealAmb,
    BlockStealCap,
    CounterMax,
    Challenge,
    NoChallenge,
    NoCard,
    Card1,
    Card2,
    KEEP1_2,
    KEEP1_3,
    KEEP1_4,
    KEEP2_3,
    KEEP2_4,
    KEEP3_4,
}

public enum CounterActionType
{
    BlockForeignAid,
    BlockAssassinate,
    BlockSteal,
}

public class CardData
{
    public CardType Card;
    public bool Open;

    public CardData()
    {
        Card = CardType.None;
        Open = false;
    }
}

public class Player
{

    public string Name { get; private set; }
    public int Index { get; private set; }
    public int Coin { get; private set; }
    CardData[] _cards = { new CardData(), new CardData()};
    Board _board;
    List<ActionType> _chooseList;
    List<int> _weightList;
    List<Player> _secondList;
    ActionType _decision;
    int _aiWaitTime;
    bool _thinkFinished;

    bool _isAI;

    public Player(Board board, int index)
    {
        _board = board;
        Index = index;
        Name = "Player" + (index + 1);
        Coin = 0;
        _chooseList = new List<ActionType>();
        _secondList = new List<Player>();
        _weightList = new List<int>();
        _isAI = Name != "Player1";
    }

    public CardData[] GetCards()
    {
        return _cards;
    }

    public void AddCard(int index, CardType card)
    {
        _cards[index].Card = card;
    }

    public void AddCoin(int count)
    {
        Coin += count;
    }

    public void StartChoose(int time)
    {
        _aiWaitTime = time;
        _thinkFinished = false;
        if(!_isAI)
        {
            _board.WaitForInput = true;
            UIBoard.instance.ShowChoice(this, _chooseList);
        }
    }
    public void ClearChooseList()
    {
        _chooseList.Clear();
        _weightList.Clear();
        _secondList.Clear();
    }
    public void AddChooseList(ActionType type, int weight)
    {
        _chooseList.Add(type);
        _weightList.Add(weight);
    }
    public void AddSecondList(Player p, int weight)
    {
        _secondList.Add(p);
        _weightList.Add(weight);
    }

    public bool IsAlive()
    {
        return !_cards[0].Open || !_cards[1].Open;
    }

    public bool IsAI()
    {
        return _isAI;
    }

    public int HasUnOpenCard(CardType card)
    {
        int count = 0;
        if (_cards[0].Card == card && !_cards[0].Open)
            count++;
        if (_cards[1].Card == card && !_cards[1].Open)
            count++;
        return count;
    }
    public bool Thinking()
    {
        if (_thinkFinished)
            return true;
        if(_isAI && !_board.WaitForInput)
        {
            _aiWaitTime -= (int)(Time.time * 1000);
            if (_aiWaitTime <= 0)
            {
                MakeAIDecision();
            }
        }


        return _thinkFinished;
    }

    void MakeAIDecision()
    {
        _thinkFinished = true;
        var index = LogicRandom.Instance.RandomSlotInWeights(_weightList);
        MakeFirstChoice(index);
        if(_secondList.Count > 0)
        {
            var idx2 = LogicRandom.Instance.RandomSlotInWeights(_weightList);
            MakeSecondChoice(idx2);
        }
    }

    public List<Player> MakeFirstChoice(int index)
    {
        _decision = _chooseList[index];
        if (_decision == ActionType.Coup || _decision == ActionType.Assassinate || _decision == ActionType.Steal)
        {
            GenerateSecondChoice(_decision);
        }
        else
        {
            _board.GetStateMgr().MakeDecision(this, _decision, null);
          }
        return _secondList;
    }
    public void MakeSecondChoice(int index)
    {
        var target = _secondList[index];
        _board.GetStateMgr().MakeDecision(this, _decision, target);
    }

    void GenerateSecondChoice(ActionType decision)
    {
        _weightList.Clear();
        _secondList.Clear();
        foreach (Player p in _board.GetPlayers())
        {
            if (p.IsAlive() && p != this)
            {
                int w = CalcSecondChoiceWeight(decision, p);
                if(w > 0)
                {
                    _secondList.Add(p);
                    _weightList.Add(w);
                }
            }
        }

    }

    int CalcSecondChoiceWeight(ActionType decision, Player target)
    {
        if(decision == ActionType.Coup)
        {
            return 1000;
        }
        else if(decision == ActionType.Assassinate)
        {
            return 1000;
        }
        else if(decision == ActionType.Steal)
        {
            return target.Coin * 1000;
        }
        return 0;
    }

    public void SetFinishInput()
    {
        _board.WaitForInput = false;
        _thinkFinished = true;
    }
    public void Print()
    {
        UIBoard.instance.UpdatePlayer(this);
        Debug.Log(string.Format("{0} coin[{1}] card[{2}{3},{4}{5}]", Name, Coin,
            _cards[0].Open ? "!" : "", _cards[0].Card, _cards[1].Open ? "!" : "", _cards[1].Card));
    }
}
