using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GameConfig
{
    public static int CARD_MAX = 3;
    public static int PLAYER_CARD_MAX = 2;
    public static int PLAYER_MAX = 6;
    public static int PLAYER_MIN = 2;
    public static int COIN_START = 2;

    public static int COIN_INCOME = 1;
    public static int COIN_FOREIGN_AID = 2;
    public static int COIN_COULD_COUP = 7;
    public static int COIN_MUST_COUP = 10;
    public static int COIN_TAX = 3;
    public static int COIN_ASSASSINATE = 3;
    public static int COIN_STEAL = 2;

    public static CardType GetNeedCard(ActionType type)
    {
        switch (type)
        {
            case ActionType.Tax:
            case ActionType.BlockForeignAid:
                return CardType.Duke;
            case ActionType.Assassinate:
                return CardType.Assassin;
            case ActionType.Steal:
            case ActionType.BlockStealCap:
                return CardType.Captain;
            case ActionType.Exchange:
            case ActionType.BlockStealAmb:
                return CardType.Ambassador;
            case ActionType.BlockAssassinate:
                return CardType.Contessa;
            default:
                Debug.LogError("wrong type to prove action " + type);
                return CardType.None;
        }
    }

}
