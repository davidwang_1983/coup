using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
public class LogicRandom {

    public static LogicRandom Instance {
        get {
            if (_inst == null)
                _inst = new LogicRandom();
            return _inst;
        }
    }
    static LogicRandom _inst;
    Random _random;
    public LogicRandom()
    {
        int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
        //seed = 1234567;
        _random = new Random(seed);
    }
    public int RandomIntInclusive(int max)
    {
        return _random.Next(max);
    }

    public int RandomIntInclusive(int min, int max)
    {
        return _random.Next(min, max);
    }

    public int RandomSlotInWeights(List<int> weights)
    {
        int totalWeight = 0;
        foreach(int weight in weights)
        {
            totalWeight += weight;
        }
        int result = RandomIntInclusive(1, totalWeight);
        int val = 0;
        for(int i = 0; i < weights.Count; ++i)
        {
            val += weights[i];
            if(val >= result)
            {
                return i;
            }
        }
        Debug.LogError("RandomSlotInWeights Failed");
        return 0;
    }

    public void Shuffle<T>(List<T> list)
    {
        var count = list.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = RandomIntInclusive(i, count - 1);
            var tmp = list[i];
            list[i] = list[r];
            list[r] = tmp;
        }
    }
}
