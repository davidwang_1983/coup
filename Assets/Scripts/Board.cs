using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CardType
{
    None,
    Duke,
    Assassin,
    Captain,
    Ambassador,
    Contessa,
}

public enum ChallengeResult
{
    Null,
    ChallengeActionFail,
    ChallengeActionSucc,
    ChallengeCounterFail,
    ChallengeCounterSucc,
}
public class RoundData
{
    public Player CurrentPlayer;
    public ActionType? ActionType;
    public Player ActionTarget;
    public Player ChallengeActionPlayer;
    public Player CounterPlayer;
    public ActionType? CounterActionType;
    public Player ChallengeCounterPlayer;
    public ChallengeResult ChallengeResult;
    public Player Winner;

    public void StartPlayer(Player player)
    {
        CurrentPlayer = player;
        ActionType = null;
        ActionTarget = null;
        ChallengeActionPlayer = null;
        CounterPlayer = null;
        CounterActionType = null;
        ChallengeCounterPlayer = null;
        ChallengeResult = ChallengeResult.Null;
        Winner = null;
        UIBoard.instance.SetCurrentPlayer(player);
    }

}

public class Board
{
    public int MaxPlayer { private set; get; }
    List<Player> _playerList;
    List<CardType> _deck;
    int _startIndex;
    StateManager _stateMgr;
    RoundData _roundData;
    public bool WaitForInput { set; get; }
    public Board(int maxPlayer)
    {
        MaxPlayer = maxPlayer;
        _deck = new List<CardType>();
        _playerList = new List<Player>();
        _stateMgr = new StateManager();
        _roundData = new RoundData();
        WaitForInput = false;
        InitDecks();
        InitPlayers();
        InitRound();
    }
    void InitDecks()
    {

        for(int i = 0; i < GameConfig.CARD_MAX; ++i)
        {
            _deck.Add(CardType.Duke);
            _deck.Add(CardType.Assassin);
            _deck.Add(CardType.Captain);
            _deck.Add(CardType.Ambassador);
            _deck.Add(CardType.Contessa);
        }
        ShuffleDeck();
    }

    void ShuffleDeck()
    {
        LogicRandom.Instance.Shuffle(_deck);
    }

    public CardType DrawCard()
    {
        if(_deck.Count == 0)
        {
            Debug.LogError("Empty Deck Error");
            return CardType.None;
        }
        var card = _deck[0];
        _deck.RemoveAt(0);
        return card;
    }

    public void ReturnCard(CardType card)
    {
        _deck.Add(card);
        ShuffleDeck();
    }

    void InitPlayers()
    {

        for (int i = 0; i < MaxPlayer; ++i)
        {
            var player = new Player(this, i);
            _playerList.Add(player);
            for (int k = 0; k < 2; ++k)
            {
                player.AddCard(k, DrawCard());
            }
        }
    }

    void InitRound()
    {
        _startIndex = LogicRandom.Instance.RandomIntInclusive(MaxPlayer - 1);
        for(int i = 0; i < MaxPlayer; ++i)
        {
            _playerList[i].AddCoin(GameConfig.COIN_START);
        }
        if(MaxPlayer == 2)
        {
            _playerList[_startIndex].AddCoin(-1);
        }
        Debug.Log("Init Round");
        foreach(var p in _playerList)
        {
            p.Print();
        }
        _stateMgr.Init(this);
        _stateMgr.ChangeToState(TurnState.TurnStart);
    }

    public List<Player> GetPlayers()
    {
        return _playerList;
    }

    public StateManager GetStateMgr()
    {
        return _stateMgr;
    }
    public Player GetNextPlayer()
    {
        while(true)
        {
            if (_startIndex == MaxPlayer)
                _startIndex = 0;
            var player = _playerList[_startIndex++];
            if (player.IsAlive())
                return player;
 
        }
    }

    public RoundData GetRoundData()
    {
        return _roundData;
    }

    public void PrintDeck()
    {
        string s = "Deck:";
        foreach(var card in _deck)
        {
            s += card.ToString() + ",";
        }
        Debug.Log(s);
    }
    public void Tick()
    {
        BaseState state = _stateMgr.GetState();
        state.Tick();
    }

    public int GetOpenCardCount(CardType card)
    {
        int count = 0;
        foreach(Player p in _playerList)
        {
            var cards = p.GetCards();
            if (cards[0].Card == card && cards[0].Open)
            {
                count++;
            }
            if (cards[1].Card == card && cards[1].Open)
            {
                count++;
            }
        }
        return count;
    }

    public void Dispose()
    {

    }
}
