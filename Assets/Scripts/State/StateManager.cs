using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurnState
{
    TurnStart,
    ChooseAction,
    ChallengeAction,
    Prove,
    Counter,
    ChallengeCounter,
    LoseInfluence,
    TurnEnd,
    Exchange,
    GameEnd,
}

public class BaseState 
{
    protected Board _board;
    protected RoundData _roundData;
    protected List<Player> _thinkingPlayers;
    public BaseState(Board board)
    {
        _board = board;
        _roundData = _board.GetRoundData();
        _thinkingPlayers = new List<Player>();
    }

    protected void SetThinkingPlayers(Player excludePlayer)
    {
        _thinkingPlayers.Clear();
        foreach (Player p in _board.GetPlayers())
        {
            if (p.IsAlive() && p != excludePlayer)
                _thinkingPlayers.Add(p);
        }
    }
    public virtual void OnStateEnter(TurnState oldState) { }
    public virtual void OnStateExit(TurnState newState) { }
    public virtual void Tick() { }
    public virtual bool IsReady() { return false; }
    public virtual void Excute() { }

    public virtual void MakeDecision(Player p, ActionType decision, Player target) { }
}

public class StateManager
{

    private Dictionary<TurnState, BaseState> _stateMap = new Dictionary<TurnState, BaseState>();

    private TurnState _currentState;
    public TurnState CurrentState
    {
        get { return _currentState; }
        set
        {
            _currentState = value;
        }
    }

    public BaseState GetState()
    {
        return _stateMap[_currentState];
    }



    public void Dispose()
    {
        _stateMap.Clear();
    }

    public void Init(Board board)
    {
        _stateMap.Add(TurnState.TurnStart, new StateTurnStart(board));
        _stateMap.Add(TurnState.ChooseAction, new StateChooseAction(board));
        _stateMap.Add(TurnState.ChallengeAction, new StateChallengeAction(board));
        _stateMap.Add(TurnState.Counter, new StateCounterAction(board));
        _stateMap.Add(TurnState.ChallengeCounter, new StateChallengeCounter(board));
        _stateMap.Add(TurnState.Prove, new StateProve(board));
        _stateMap.Add(TurnState.LoseInfluence, new StateLoseInfluence(board));
        _stateMap.Add(TurnState.TurnEnd, new StateTurnEnd(board));
        _stateMap.Add(TurnState.Exchange, new StateExchange(board));
        _stateMap.Add(TurnState.GameEnd, new StateGameEnd(board));

        CurrentState = TurnState.GameEnd;
    }

    public void Tick()
    {
        _stateMap[_currentState].Tick();
    }

    public void ChangeToState(TurnState newState)
    {
        TurnState targetState = newState;
        TurnState oldState = _currentState;
        _stateMap[_currentState].OnStateExit(targetState);
        CurrentState = targetState;
        _stateMap[targetState].OnStateEnter(oldState);
    }

    public void MakeDecision(Player p, ActionType decision, Player target)
    {
        _stateMap[_currentState].MakeDecision(p, decision, target);
    }
}