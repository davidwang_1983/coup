using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class StateGameEnd : BaseState
{
    public StateGameEnd(Board board) : base(board)
    {

    }
    public override void OnStateEnter(TurnState oldState)
    {
        Debug.LogError(string.Format("GameEnd {0} is Winner", _roundData.Winner.Name));
        Game.instance.EndGame();
    }
}
