using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateLoseInfluence : BaseState
{
    Player losePlayer;
    TurnState nextState;
    public StateLoseInfluence(Board board) : base(board)
    {
    }
    public override void OnStateEnter(TurnState oldState)
    {
        Debug.Log("enter lose influence");
        if (oldState == TurnState.TurnEnd)
        {
            losePlayer = _roundData.ActionTarget;
            nextState = TurnState.TurnStart;
        }
        else if (_roundData.ChallengeResult == ChallengeResult.ChallengeActionFail)
        {
            losePlayer = _roundData.ChallengeActionPlayer;
            nextState = TurnState.Counter;
        }
        else if (_roundData.ChallengeResult == ChallengeResult.ChallengeActionSucc)
        {
            losePlayer = _roundData.CurrentPlayer;
            nextState = TurnState.TurnEnd;
        }
        else if (_roundData.ChallengeResult == ChallengeResult.ChallengeCounterSucc)
        {
            losePlayer = _roundData.CounterPlayer;
            nextState = TurnState.TurnEnd;
        }
        else if (_roundData.ChallengeResult == ChallengeResult.ChallengeCounterFail)
        {
            losePlayer = _roundData.ChallengeCounterPlayer;
            nextState = TurnState.TurnEnd;
        }
        else
        {
            Debug.LogError("unexpected result in loseInfluence " + _roundData.ChallengeResult + " " + oldState);
            return;
        }



        var cards = losePlayer.GetCards();
        if(!cards[0].Open && !cards[1].Open)
        {
            losePlayer.ClearChooseList();
            losePlayer.AddChooseList(ActionType.Card1, 1000);
            losePlayer.AddChooseList(ActionType.Card2, 1000);
            losePlayer.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 1000));
        }
        else
        {
            if (cards[0].Open)
            {
                cards[1].Open = true;
            }
            else
            {
                cards[0].Open = true;
            }
            Debug.LogError(string.Format("{0} out of game", losePlayer.Name));
            losePlayer.Print();
            var winner = GetWinner();
            if(winner != null)
            {
                _roundData.Winner = winner;
                _board.GetStateMgr().ChangeToState(TurnState.GameEnd);
            }
            else
            {
                _board.GetStateMgr().ChangeToState(nextState);
            }
            
        }

    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit lose influence Action");
    }
    public override void Tick()
    {
        if (losePlayer == null)
            return;
        if(losePlayer.Thinking())
        {
            _board.GetStateMgr().ChangeToState(nextState);
        }

    }

    Player GetWinner()
    {
        var players = _board.GetPlayers();
        Player alivePlayer = null;
        foreach(var p in players)
        {
            if (p.IsAlive())
            {
                if (alivePlayer == null)
                {
                    alivePlayer = p;
                }
                else
                {
                    return null;
                }
            }
        }
        return alivePlayer;
    }
    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        var cards = losePlayer.GetCards();
        CardType cardType = CardType.None;
        if (decision == ActionType.Card1)
        {
            cards[0].Open = true;
            cardType = cards[0].Card;
        }
        else if(decision == ActionType.Card2)
        {
            cards[1].Open = true;
            cardType = cards[1].Card;
        }
        Debug.LogError(string.Format("{0} lose one Influence {1}", losePlayer.Name, cardType));
        losePlayer.Print();
    }
}
