using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class StateExchange : BaseState
{
    CardType card3;
    CardType card4;
    public StateExchange(Board board) : base(board)
    {

    }

    public override void OnStateEnter(TurnState oldState)
    {
        Debug.Log("Enter Exchange Action:");
        _board.PrintDeck();
        var player = _roundData.CurrentPlayer;
        player.ClearChooseList();
        var cards = player.GetCards();
        card3 = _board.DrawCard();
        card4 = _board.DrawCard();

        if (!player.IsAI())
            Debug.LogError(string.Format("Exchange Card3 {0} card4 {1}", card3, card4));
        
        if (!cards[0].Open && !cards[1].Open)
        {
            player.AddChooseList(ActionType.KEEP1_2, 1000);
            player.AddChooseList(ActionType.KEEP1_3, 1000);
            player.AddChooseList(ActionType.KEEP1_4, 1000);
            player.AddChooseList(ActionType.KEEP2_3, 1000);
            player.AddChooseList(ActionType.KEEP2_4, 1000);
            player.AddChooseList(ActionType.KEEP3_4, 1000);
        }
        else if(cards[0].Open)
        {
            player.AddChooseList(ActionType.KEEP1_2, 1000);
            player.AddChooseList(ActionType.KEEP1_3, 1000);
            player.AddChooseList(ActionType.KEEP1_4, 1000);
        }
        else if (cards[1].Open)
        {
            player.AddChooseList(ActionType.KEEP1_2, 1000);
            player.AddChooseList(ActionType.KEEP2_3, 1000);
            player.AddChooseList(ActionType.KEEP2_4, 1000);
        }
        player.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 1000));
    }

    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit Exchange Action");
    }

    public override void Tick()
    {
        if (_roundData.CurrentPlayer.Thinking())
        {
        }

    }
    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        var cards = p.GetCards();
        var oldCard1 = cards[0].Card;
        var oldCard2 = cards[1].Card;
        switch (decision)
        {
            case ActionType.KEEP1_2:
                _board.ReturnCard(card3);
                _board.ReturnCard(card4);
                break;
            case ActionType.KEEP1_3:
                cards[1].Card = card3;
                _board.ReturnCard(oldCard2);
                _board.ReturnCard(card4);
                break;
            case ActionType.KEEP1_4:
                cards[1].Card = card4;
                _board.ReturnCard(oldCard2);
                _board.ReturnCard(card3);
                break;
            case ActionType.KEEP2_3:
                cards[0].Card = card4;
                _board.ReturnCard(oldCard1);
                _board.ReturnCard(card4);
                break;
            case ActionType.KEEP2_4:
                cards[0].Card = card3;
                _board.ReturnCard(oldCard1);
                _board.ReturnCard(card3);
                break;
            case ActionType.KEEP3_4:
                cards[0].Card = card3;
                cards[1].Card = card4;
                _board.ReturnCard(oldCard1);
                _board.ReturnCard(oldCard2);
                break;
        }

        Debug.LogError(string.Format("{0} Exchange excuted", p.Name));
        Debug.Log("Exchange result:");
        p.Print();
        _board.PrintDeck();
        _board.GetStateMgr().ChangeToState(TurnState.TurnStart);
    }
}
