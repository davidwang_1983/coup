using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateChallengeCounter : BaseState
{
    List<ActionType> validType;
    public StateChallengeCounter(Board board) : base(board)
    {
        validType = new List<ActionType>();
        validType.Add(ActionType.BlockForeignAid);
        validType.Add(ActionType.BlockAssassinate);
        validType.Add(ActionType.BlockStealAmb);
        validType.Add(ActionType.BlockStealCap);
    }

    public override void OnStateEnter(TurnState oldState)
    {
        //Debug.Log("Enter Challenge Counter");
        if (validType.Contains(_roundData.CounterActionType.Value))
        {
            SetThinkingPlayers(_roundData.CounterPlayer);
            foreach (Player p in _thinkingPlayers)
            {
                TriggerChallengeCounterState(p);
            }
        }
        else
        {
            _board.GetStateMgr().ChangeToState(TurnState.TurnEnd);
        }
    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit Challenge Counter");
    }
    public override void Tick()
    {
        bool allFinished = true;
        foreach (Player p in _thinkingPlayers)
        {
            if (!p.Thinking())
            {
                allFinished = false;
            }
        }

        if (allFinished)
        {
            if (_roundData.ChallengeCounterPlayer == null)
            {
                _board.GetStateMgr().ChangeToState(TurnState.TurnEnd);
            }
            else
            {
                _board.GetStateMgr().ChangeToState(TurnState.Prove);
            }
        }
    }
    void TriggerChallengeCounterState(Player p)
    {
        p.ClearChooseList();
        int weight = GetChallangeActionWeight(p, _roundData.CounterActionType.Value, _roundData.CounterPlayer);
        if (weight > 0)
        {
            p.AddChooseList(ActionType.Challenge, weight);
            p.AddChooseList(ActionType.NoChallenge, 1000);
        }
        p.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 5000));
    }

    int GetChallangeActionWeight(Player p, ActionType targetType, Player target)
    {
        CardType needCard = GameConfig.GetNeedCard(targetType);
        if (needCard != CardType.None)
        {
            return CalcChallengeWeight(needCard, p, target);
        }
        return 0;
    }
    int CalcChallengeWeight(CardType needCard, Player p, Player target)
    {
        int weight = p == target ? 1000 : 200;

        int count = _board.GetOpenCardCount(needCard);
        count += p.HasUnOpenCard(needCard);
        if (count >= 3)
            return 1000000;
        else
            return weight * (1000 + count * 500) / 1000;
    }
    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        if (decision == ActionType.Challenge)
        {
            if(_roundData.ChallengeCounterPlayer == null)
            {
                _roundData.ChallengeCounterPlayer = p;
                Debug.LogError(string.Format("{0} Challenge Counter {1} against {2}", p.Name, _roundData.CounterActionType.Value, _roundData.CounterPlayer.Name));
            }

        }
    }
}
