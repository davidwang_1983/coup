using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateCounterAction : BaseState
{
    List<ActionType> validType;
    public StateCounterAction(Board board) : base(board)
    {
        validType = new List<ActionType>();
        validType.Add(ActionType.ForeignAid);
        validType.Add(ActionType.Assassinate);
        validType.Add(ActionType.Steal);
    }

    public override void OnStateEnter(TurnState oldState)
    {
        //Debug.Log("Enter Counter Action");
        _roundData.CurrentPlayer.Print();
        var type = _roundData.ActionType.Value;
        _thinkingPlayers.Clear();
        if (validType.Contains(type))
        {
            if (type == ActionType.ForeignAid)
            {
                SetThinkingPlayers(_roundData.CurrentPlayer);
                foreach (Player p in _thinkingPlayers)
                {
                    TriggerCounterActionState(p);
                }
            }
            else
            {
                if(_roundData.ActionTarget.IsAlive())
                {
                    _thinkingPlayers.Add(_roundData.ActionTarget);
                    TriggerCounterActionState(_roundData.ActionTarget);
                }

            }
        }
        if(_thinkingPlayers.Count == 0)
        {
            _board.GetStateMgr().ChangeToState(TurnState.TurnEnd);
        }
    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit Counter Action");
    }
    public override void Tick()
    {
        bool allFinished = true;
        foreach (Player p in _thinkingPlayers)
        {
            if (!p.Thinking())
            {
                allFinished = false;
            }
        }

        if (allFinished)
        {
            if (_roundData.CounterPlayer != null)
            {
                _board.GetStateMgr().ChangeToState(TurnState.ChallengeCounter);
            }
            else
            {
                _board.GetStateMgr().ChangeToState(TurnState.TurnEnd);
            }
        }
    }


    void TriggerCounterActionState(Player p)
    {
        p.ClearChooseList();
        for(ActionType counter = ActionType.CounterStart + 1; counter < ActionType.CounterMax; counter++)
        {
            int weight = GetCounterActionWeight(p, _roundData.ActionType.Value, counter);
            if (weight > 0)
            {
                p.AddChooseList(counter, weight);
            }
        }
        p.AddChooseList(ActionType.NoCounter, 1000);
        p.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 5000));
    }



    int GetCounterActionWeight(Player player, ActionType actionType, ActionType counterType)
    {
        if (counterType == ActionType.BlockForeignAid && actionType == ActionType.ForeignAid)
            return CalcCounterActionWeight(counterType, player);
        else if (counterType == ActionType.BlockAssassinate && actionType == ActionType.Assassinate)
            return CalcCounterActionWeight(counterType, player);
        else if (counterType == ActionType.BlockStealAmb && actionType == ActionType.Steal)
            return CalcCounterActionWeight(counterType, player);
        else if (counterType == ActionType.BlockStealCap && actionType == ActionType.Steal)
            return CalcCounterActionWeight(counterType, player);
        return 0;
    }

    int CalcCounterActionWeight(ActionType counterType, Player p)
    {
        int weight = 200;
        CardType card = GameConfig.GetNeedCard(counterType);
        var count = _board.GetOpenCardCount(card);
        if (count == 3)
        {
            return 0;
        }
        else
        {
            weight = weight - 50 * count;
        }

        if (p.HasUnOpenCard(card) > 0)
        {
            weight += 500;
        }

        return weight;
    }
    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        if (decision != ActionType.NoCounter)
        {
            if(_roundData.CounterPlayer == null)
            {
                _roundData.CounterPlayer = p;
                _roundData.CounterActionType = decision;
                Debug.LogError(string.Format("{0} Counter {1} against {2}", p.Name, decision, _roundData.CurrentPlayer.Name));
                p.Print();
            }

        }
    }
}
