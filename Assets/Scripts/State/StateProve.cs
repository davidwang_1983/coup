using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateProve : BaseState
{
    Player provePlayer;
    Player challengePlayer;
    TurnState originState;
    public StateProve(Board board) : base(board)
    {

    }

    public override void OnStateEnter(TurnState oldState)
    {
        //Debug.Log("enter prove Action");

        if (oldState == TurnState.ChallengeAction)
        {
            provePlayer = _roundData.CurrentPlayer;
            challengePlayer = _roundData.ChallengeActionPlayer;
            originState = oldState;
            TriggerProveAction(provePlayer, _roundData.ActionType.Value);
        }
        else if(oldState == TurnState.ChallengeCounter)
        {
            provePlayer = _roundData.CounterPlayer;
            challengePlayer = _roundData.ChallengeCounterPlayer;
            originState = oldState;
            TriggerProveAction(provePlayer, _roundData.CounterActionType.Value);
        }
        else
        {
            Debug.LogError("wrong state in prove " + oldState);
            return;
        }

    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit prove Action");
    }
    public override void Tick()
    {
        if (provePlayer.Thinking())
        {

        }
    }

    void TriggerProveAction(Player p, ActionType proveAction)
    {
        p.ClearChooseList();
        CardType needCard = GameConfig.GetNeedCard(proveAction);
        var cards = p.GetCards();
        p.AddChooseList(ActionType.NoCard, 200);
        for (int i = 0; i < cards.Length; ++i)
        {
            if (cards[i].Open)
                continue;
            var actionType = i == 0 ? ActionType.Card1 : ActionType.Card2;
            if(cards[i].Card == needCard)
                p.AddChooseList(actionType, 1000);
        }
        p.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 1000));
    }


    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        var cards = p.GetCards();
        bool bPassed = false;
        if(decision == ActionType.Card1 || decision == ActionType.Card2)
        {
            bPassed = true;

        }

        if(bPassed)
        {
            int index = decision == ActionType.Card1 ? 0 : 1;
            CardType card = cards[index].Card;
            _board.ReturnCard(cards[index].Card);
            cards[index].Card = _board.DrawCard();
            _roundData.ChallengeResult = originState == TurnState.ChallengeAction ? ChallengeResult.ChallengeActionFail : ChallengeResult.ChallengeCounterFail;
            Debug.LogError(string.Format("{0} succeed prove card {1}", p.Name, card));
            p.Print();
            _board.PrintDeck();
            _board.GetStateMgr().ChangeToState(TurnState.LoseInfluence);

        }
        else
        {
            Debug.LogError(string.Format("{0} failed prove card", p.Name));
            _roundData.ChallengeResult = originState == TurnState.ChallengeAction ? ChallengeResult.ChallengeActionSucc : ChallengeResult.ChallengeCounterSucc;
            _board.GetStateMgr().ChangeToState(TurnState.LoseInfluence);
        }
    }
}
