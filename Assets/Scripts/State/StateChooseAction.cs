using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class StateChooseAction : BaseState
{
    public StateChooseAction(Board board) : base(board)
    {
        
    }

    public override void OnStateEnter(TurnState oldState)
    {
        //Debug.Log("Enter Choose Action");
        _roundData.StartPlayer(_board.GetNextPlayer());
        Debug.Log(string.Format("{0} start turn:", _roundData.CurrentPlayer.Name));
        _roundData.CurrentPlayer.Print();
        TriggerChooseActionState(_roundData.CurrentPlayer);
    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit Choose Action");
    }
    public override void Tick()
    {
        if(_roundData.CurrentPlayer.Thinking())
        {
            if (_roundData.ActionType != null)
            {
                _board.GetStateMgr().ChangeToState(TurnState.ChallengeAction);
            }
        }

    }

    void TriggerChooseActionState(Player p)
    {
        p.ClearChooseList();

        if (p.Coin >= GameConfig.COIN_MUST_COUP)
        {
            p.AddChooseList(ActionType.Coup, 1000);
        }
        else
        {
            for (ActionType t = ActionType.ChooseStart + 1; t < ActionType.ChooseMax; t++)
            {
                var weight = GetChooseActionWeight(t, p);
                if (weight > 0)
                {
                    p.AddChooseList(t, weight);
                }
            }
        }
        p.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 1000));
    }

    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        _roundData.ActionType = decision;
        _roundData.ActionTarget = target;
        if(target != null)
        {
            Debug.LogError(string.Format("{0} choose {1} against {2}", p.Name, decision, target.Name));
        }
        else
        {
            Debug.LogError(string.Format("{0} choose {1}", p.Name, decision));
        }
    }
    int GetChooseActionWeight(ActionType type, Player p)
    {
        switch (type)
        {
            case ActionType.Income:
                return 1000;
            case ActionType.ForeignAid:
                return 1000;
            case ActionType.Coup:
                return p.Coin >= GameConfig.COIN_COULD_COUP ? 1000 * (p.Coin - GameConfig.COIN_COULD_COUP + 1) : 0;
            case ActionType.Tax:
                return CalcChracterActionWeight(1000, type, p);
            case ActionType.Assassinate:
                if(p.Coin >= GameConfig.COIN_ASSASSINATE)
                    return CalcChracterActionWeight(1000, type, p);
                return 0;
            case ActionType.Steal:
                foreach(var otherPlayer in _board.GetPlayers())
                {
                    if(otherPlayer != p && otherPlayer.Coin > 0 && otherPlayer.IsAlive())
                    {
                        return CalcChracterActionWeight(1000, type, p);
                    }
                }
                return 0;
            case ActionType.Exchange:
                return CalcChracterActionWeight(1000, type, p);
            default:
                return 0;
        }
    }

    int CalcChracterActionWeight(int baseWeight, ActionType type, Player p)
    {
        int weight = baseWeight;
        CardType card = GameConfig.GetNeedCard(type);
        var count = _board.GetOpenCardCount(card);
        if(count == 3)
        {
            return 0;
        }
        else
        {
            weight = weight - 333 * count;
        }

        if(p.HasUnOpenCard(card) > 0)
        {
            weight += 1500;
        }

        return weight;
    }
}
