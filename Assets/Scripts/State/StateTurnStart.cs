using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTurnStart : BaseState
{
    public StateTurnStart(Board board) : base(board)
    {

    }

    public override void OnStateEnter(TurnState oldState)
    {
        Debug.Log("Enter Turn Start");
        //_board.GetStateMgr().ChangeToState(TurnState.ChooseAction);
    }

    public void TestContinue()
    {
        _board.GetStateMgr().ChangeToState(TurnState.ChooseAction);
    }
}