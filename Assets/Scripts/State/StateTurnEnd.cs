using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTurnEnd : BaseState
{
    public StateTurnEnd(Board board) : base(board)
    {

    }

    public override void OnStateEnter(TurnState oldState)
    {
        //Debug.Log("Enter Turn End");
        bool bActionSucceed = IsActionSucceed();

        if(bActionSucceed)
        {
            TriggerAction();
        }
        else
        {
            if(_roundData.ActionType.Value == ActionType.Assassinate)
            {
                _roundData.CurrentPlayer.AddCoin(-GameConfig.COIN_ASSASSINATE);
            }
            _board.GetStateMgr().ChangeToState(TurnState.TurnStart);
        }

    }

    bool IsActionSucceed()
    {
        if(_roundData.CounterPlayer != null)
        {
            if (_roundData.ChallengeCounterPlayer != null && _roundData.ChallengeResult == ChallengeResult.ChallengeCounterFail)
                return false;
            if (_roundData.ChallengeCounterPlayer == null)
                return false;
        }
        if(_roundData.ChallengeActionPlayer != null && _roundData.ChallengeResult == ChallengeResult.ChallengeActionSucc)
        {
            return false;
        }
        return true;
    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit Turn End");
    }
    public override void Tick() { }

    void TriggerAction()
    {
        var player = _roundData.CurrentPlayer;
        var target = _roundData.ActionTarget;
        var nextTurn = TurnState.TurnStart;
        if (_roundData.ActionType == ActionType.Coup)
        {
            player.AddCoin(-GameConfig.COIN_COULD_COUP);
            nextTurn =TurnState.LoseInfluence;
            Debug.LogError(string.Format("{0} Coup {1}", player.Name, target.Name));
        }
        else if(_roundData.ActionType == ActionType.Assassinate)
        {
            player.AddCoin(-GameConfig.COIN_ASSASSINATE);
            nextTurn = TurnState.LoseInfluence;
            Debug.LogError(string.Format("{0} Assassinate {1}", player.Name, target.Name));
        }
        else if(_roundData.ActionType == ActionType.Exchange)
        {
            nextTurn = TurnState.Exchange;
        }
        else
        {
            switch (_roundData.ActionType)
            {
                case ActionType.Income:
                    player.AddCoin(GameConfig.COIN_INCOME);
                    Debug.LogError(string.Format("{0} {1} excuted", player.Name, _roundData.ActionType));
                    break;
                case ActionType.ForeignAid:
                    player.AddCoin(GameConfig.COIN_FOREIGN_AID);
                    Debug.LogError(string.Format("{0} {1} excuted", player.Name, _roundData.ActionType));
                    break;
                case ActionType.Tax:
                    player.AddCoin(GameConfig.COIN_TAX);
                    Debug.LogError(string.Format("{0} {1} excuted", player.Name, _roundData.ActionType));
                    break;
                case ActionType.Steal:
                    var coin = Math.Min(target.Coin, GameConfig.COIN_STEAL);
                    player.AddCoin(coin);
                    target.AddCoin(-coin);
                    Debug.LogError(string.Format("{0} Steal Coin{1} from{2}", player.Name, coin, target.Name));
                    break;
                default:
                    break;
            }
            
        }

        Debug.Log("after turn End:");
        player.Print();
        if(target != null)
        {
            Debug.Log("target turn End:");
            target.Print();
        }
        _board.GetStateMgr().ChangeToState(nextTurn);

    }
}
