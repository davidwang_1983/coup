using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class StateChallengeAction : BaseState
{
    List<ActionType> validType;
    public StateChallengeAction(Board board) : base(board)
    {
        validType = new List<ActionType>();
        validType.Add(ActionType.Tax);
        validType.Add(ActionType.Assassinate);
        validType.Add(ActionType.Steal);
        validType.Add(ActionType.Exchange);

    }

    public override void OnStateEnter(TurnState oldState)
    {
        //Debug.Log("Enter Challenge Action");
        if (validType.Contains(_roundData.ActionType.Value))
        {
            SetThinkingPlayers(_roundData.CurrentPlayer);
            foreach(Player p in _thinkingPlayers)
            {
                TriggerChallengeActionState(p);
            }
        }
        else
        {
            _board.GetStateMgr().ChangeToState(TurnState.Counter);
        }
    }
    public override void OnStateExit(TurnState newState)
    {
        //Debug.Log("Exit Challenge Action");
    }
    public override void Tick()
    {
        bool allFinished = true;
        foreach(Player p in _thinkingPlayers)
        {
            if(!p.Thinking())
            {
                allFinished = false;
            }
        }

        if (_roundData.ChallengeActionPlayer != null)
        {
            _board.GetStateMgr().ChangeToState(TurnState.Prove);
        }
        else if (allFinished)
        {
            _board.GetStateMgr().ChangeToState(TurnState.Counter);
        }
    }
    void TriggerChallengeActionState(Player p)
    {
        p.ClearChooseList();
        
        int weight = GetChallangeActionWeight(p, _roundData.ActionType.Value);
        if (weight > 0)
        {
            p.AddChooseList(ActionType.Challenge, weight);
            p.AddChooseList(ActionType.NoChallenge, 1000);
        }
        p.StartChoose(LogicRandom.Instance.RandomIntInclusive(500, 5000));
    }

    int GetChallangeActionWeight(Player p, ActionType targetType)
    {
        CardType needCard = GameConfig.GetNeedCard(targetType);
        if(needCard != CardType.None)
        {
            return CalcChallengeWeight(needCard, p);
        }
        return 0;
    }

    int CalcChallengeWeight(CardType needCard, Player p)
    {
        int count = _board.GetOpenCardCount(needCard);
        count += p.HasUnOpenCard(needCard);
        if (count >= 3)
            return 1000000;
        else
            return 200 + count * 500;
    }

    public override void MakeDecision(Player p, ActionType decision, Player target)
    {
        if (decision == ActionType.Challenge)
        {
            if(_roundData.ChallengeActionPlayer == null)
            {
                _roundData.ChallengeActionPlayer = p;
                Debug.LogError(string.Format("{0} Challenge {1} against {2}", p.Name, _roundData.ActionType.Value, _roundData.CurrentPlayer.Name));
            }

        }
    }
}
